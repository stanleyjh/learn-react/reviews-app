# [Reviews](https://stanleyjh.gitlab.io/learn-react/reviews-app)

This application shows a list of reviews from people. You can cycle through the reviews and click the "Surprise Me" button to see a random reviewer.

The data is referenced locally in the data.js file. The data is imported as a `people` variable in the Review component. A state hook is used to store and update the index value for `people`. The initial render of the page displays the first object in the `people` array. Clicking the next button or previous button increases/decreases the index. The updated index is passed as an argument into the `setIndex` function which re-renders the page. There is a function called `checkNumber` which evaluates the index each time it is updated to make sure the index is not out of scope. Clicking the [Surprise me] button invokes the `randomPerson` function which uses the Math.random() method to return a random number. `setIndex` is also invoked within `randomPerson` to make sure the index is within scope.

## References

[John Smilga's Udemy React Course](https://www.udemy.com/course/react-tutorial-and-projects-course/?referralCode=FEE6A921AF07E2563CEF)

[React Icons Library](https://react-icons.github.io/react-icons/)
