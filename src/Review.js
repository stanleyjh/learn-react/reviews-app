import React, { useState } from "react";
import people from "./data";
import { FaChevronLeft, FaChevronRight, FaQuoteRight } from "react-icons/fa";

const Review = () => {
  // useState used to track and update the index. The initial state is 0 for the first element in the array.
  const [index, setIndex] = useState(0);
  // Destructuring an object in the people array.
  const { name, job, image, text } = people[index];

  console.log("index " + index);

  // Evaluate the index and check if the number is out of scope.
  const checkNumber = (number) => {
    if (number < 0) {
      return people.length - 1;
    } else if (number > people.length - 1) {
      return 0;
    }
    return number;
  };

  const previousPerson = () => {
    setIndex((index) => {
      let newIndex = index - 1;
      console.log("minus " + newIndex);
      // Returns the new index used for the people array.
      return checkNumber(newIndex);
    });
  };

  const nextPerson = () => {
    setIndex((index) => {
      let newIndex = index + 1;
      console.log("add " + newIndex);
      // Returns the new index used for the people array.
      return checkNumber(newIndex);
    });
  };

  const randomPerson = () => {
    // people.length will be 4 and we multiply it to math.random to get random numbers between 0 and 4. We want random numbers to be from 0 - 3, so we use Math.floor() to round down the number.
    let randomNumber = Math.floor(Math.random() * people.length);

    // This corrects the issue if a user clicks [Surprise Me] and it returns the same person.
    if (randomNumber === index) {
      randomNumber = index + 1;
    }

    // We want to invoke checkNumber() before setIndex() because the randomNumber may be out of scope from the if statement above.
    setIndex(checkNumber(randomNumber));
  };

  return (
    <article className="review">
      <div className="img-container">
        <img src={image} alt={name} className="person-img" />
        <span className="quote-icon">
          <FaQuoteRight />
        </span>
      </div>
      <h4 className="author">{name}</h4>
      <p className="job">{job}</p>
      <p className="info">{text}</p>
      <div className="button-container">
        <button className="prev-btn" onClick={previousPerson}>
          <FaChevronLeft />
        </button>
        <button className="next-btn" onClick={nextPerson}>
          <FaChevronRight />
        </button>
      </div>
      <button className="random-btn" onClick={randomPerson}>
        surprise me
      </button>
    </article>
  );
};

export default Review;
