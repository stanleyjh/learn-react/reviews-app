const reviews = [
  {
    id: 1,
    name: "susan smith",
    job: "web developer",
    image:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg",
    text: "Aute commodo incididunt amet non amet eu reprehenderit adipisicing et fugiat. Ea ut exercitation esse esse adipisicing fugiat in nulla tempor incididunt consequat consectetur non. Culpa labore nisi amet qui laboris elit Lorem sint sint. ",
  },
  {
    id: 2,
    name: "anna johnson",
    job: "web designer",
    image:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg",
    text: "Sit laborum do eu nisi exercitation labore sit dolor officia. Amet sit sunt deserunt sunt fugiat officia excepteur Lorem id Lorem reprehenderit. Lorem nisi ad aute est fugiat elit est minim consequat proident in elit aute. In anim sunt tempor irure anim consectetur. ",
  },
  {
    id: 3,
    name: "peter jones",
    job: "intern",
    image:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg",
    text: "Eu consequat ex nostrud pariatur esse id. Est officia id aute in fugiat ex. Voluptate sit magna quis aute consequat in proident tempor magna officia. Officia ex dolor mollit excepteur fugiat id mollit officia mollit mollit cillum commodo excepteur. ",
  },
  {
    id: 4,
    name: "bill anderson",
    job: "Manager",
    image:
      "https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg",
    text: "Mollit duis qui ut velit aliquip minim reprehenderit pariatur. Dolor excepteur esse id consequat sint velit ad. Adipisicing in fugiat tempor aliqua exercitation aute. Id nulla est mollit occaecat nostrud aute sint ut aliqua tempor in veniam. ",
  },
];

export default reviews;
